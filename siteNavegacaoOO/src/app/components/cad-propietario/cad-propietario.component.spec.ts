import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadPropietarioComponent } from './cad-propietario.component';

describe('CadPropietarioComponent', () => {
  let component: CadPropietarioComponent;
  let fixture: ComponentFixture<CadPropietarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadPropietarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadPropietarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
