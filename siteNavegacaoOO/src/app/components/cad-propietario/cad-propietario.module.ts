import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadPropietarioRoutingModule } from './cad-propietario-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadPropietarioRoutingModule
  ]
})
export class CadPropietarioModule { }
